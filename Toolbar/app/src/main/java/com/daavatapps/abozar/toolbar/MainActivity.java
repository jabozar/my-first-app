package com.daavatapps.abozar.toolbar;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.daavatapps.abozar.toolbar.adapters.DavaatFeaturesAdapter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    TextView txtTitleApp, txtTitlePortal;
    TextView txtDescription1;
    TextView txtDescription2;

    Button buttonLoadPicture;
    ImageView imgView;
    private static int RESULT_LOAD_IMG = 1;
    String imgDecodableString;


    ListView lvCustomList;
    Context context = MainActivity.this;
    ArrayList<ListData> myList = new ArrayList<ListData>();
    String[] title = new String[]{"اپلیکیشن", "پورتال سازمانی", "محتوای دیجیتال", "خدمات دیتا سنتر",
            "کسب و کار دیجیتال", "برند سازی دیجیتال"};

    String[] desc = new String[]{" تحلیل،طراحی و تولید", " فروشگاههای اینترنتی و وب سرویس", "عرضه و بروز رسانی محتوای دیجیتال", "نگهداری و توسعه دیتا سنتر",
            "طراحی مدل و برنامه کسب و کار", "طراحی استراتژی برند سازی دیجیتال"};
    int[] img = new int[]{R.drawable.application, R.drawable.portal, R.drawable.payesh, R.drawable.datacenter,
            R.drawable.digital, R.drawable.branding};


    DavaatFeaturesAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);


        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Vazir-Bold.ttf");
        TextView txtTitlebar = (TextView) findViewById(R.id.txtTitlebar);
        txtTitlebar.setTypeface(font);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true); // back button
        // getSupportActionBar().setHomeAsUpIndicator(R.drawable.menu2); // taghire button back
        //getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu); // taghire button back

        lvCustomList = (ListView) findViewById(R.id.lvCustomList);
        getDataInList();
        adapter = new DavaatFeaturesAdapter(context, myList);
        lvCustomList.setAdapter(adapter);


        lvCustomList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(parent.getContext(), ShowCardsDetailes.class);
                intent.putExtra("image_drawable_id", myList.get(position).getImage());
                intent.putExtra("title_id", myList.get(position).getTitle());
                intent.putExtra("description1_id", myList.get(position).getDescription1());
                context.startActivity(intent);

            }
        });

        drawerLayout = (DrawerLayout) findViewById(R.id.dl);
        navigationView = (NavigationView) findViewById(R.id.nv);
        navigationView.setItemIconTintList(null);


        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                int id = item.getItemId();

                if (id == R.id.setting) {
                    Toast.makeText(getBaseContext(), "تنظیمات", Toast.LENGTH_SHORT).show();

                } else if (id == R.id.about) {
                    Toast.makeText(getBaseContext(), "درباره ما", Toast.LENGTH_SHORT).show();
                } else if (id == R.id.contactus) {
                    Toast.makeText(getBaseContext(), "ارتباط با ما", Toast.LENGTH_SHORT).show();
                } else if (id == R.id.exit) {
                    finish();
                }

                drawerLayout.closeDrawer(Gravity.RIGHT);
                return true;
            }
        });


        lvCustomList.setTranscriptMode(lvCustomList.TRANSCRIPT_MODE_ALWAYS_SCROLL); // scrolled listview
        //lvCustomList.setStackFromBottom(true); // agar true bashad dar hengam load shodan az tahe list namayesh dade mishavad

        lvCustomList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                removeItemFromList(position);

                return true;
            }
        });


    }


    public void removeItemFromList(int position) {
        final int deletePosition = position;
        AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
        alert.setTitle("حدف");
        alert.setMessage("آیا میخواهید آیتم از لیست حذف شود");
        alert.setPositiveButton("بله", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                myList.remove(deletePosition);
                adapter.notifyDataSetChanged();
                adapter.notifyDataSetInvalidated();
            }
        });
        alert.setNegativeButton("خیر", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });
        alert.show();
    }


    public void onClickToAdd(View v) {

        FloatingActionButton fabAddItem = (FloatingActionButton) findViewById(R.id.fabAddItem);
        final AlertDialog.Builder alertAddItem = new AlertDialog.Builder(this);
        final LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.activity_add_item_dialog, null);
        alertAddItem.setView(dialogView);
        //alertAddItem.setCancelable(false);

        alertAddItem.setPositiveButton("ذخیره", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                final ListData data = new ListData();
                EditText etTitle = (EditText) dialogView.findViewById(R.id.etTitle);
                data.setTitle(etTitle.getText().toString());
                EditText etDescription = (EditText) dialogView.findViewById(R.id.etDesc1);
                data.setDescription1(etDescription.getText().toString());



                adapter.addNewItemToList(data);


            }

        });


        alertAddItem.setNegativeButton("لغو", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        imgView = (ImageView) dialogView.findViewById(R.id.imgView);
        buttonLoadPicture = (Button) dialogView.findViewById(R.id.buttonLoadPicture);
        buttonLoadPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, RESULT_LOAD_IMG);

            }
        });





        alertAddItem.create();
        alertAddItem.show();


    }


    private void getDataInList() {
        for (int i = 0; i < 6; i++) {

            ListData ld = new ListData();
            ld.setTitle(title[i]);
            ld.setDescription1(desc[i]);
            ld.setImage(img[i]);
            myList.add(ld);
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            drawerLayout.openDrawer(Gravity.RIGHT);
        }
        if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
            drawerLayout.closeDrawer(Gravity.RIGHT);

        }
        if (id == android.R.id.home) {
            finish(); // vaghti ru button back click shod az barname exit shavad
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
            drawerLayout.closeDrawer(Gravity.RIGHT);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {

            if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK
                    && null != data) {


                Uri selectedImage = data.getData();
                String[] filePathColumn = { MediaStore.Images.Media.DATA };

                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);

                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                imgDecodableString = cursor.getString(columnIndex);
                cursor.close();

                imgView.setImageBitmap(BitmapFactory
                        .decodeFile(imgDecodableString));

            } else {
                Toast.makeText(this, "You haven't picked Image",
                        Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();
            Log.e("ZZZ", "onActivityResult: ", e);
        }

    }





}

