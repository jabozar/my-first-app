package com.daavatapps.abozar.toolbar;


import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class AddItemDialog extends AppCompatActivity {
    EditText editText;
    ImageView importImage;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item_dialog);


        TextView textViewTitle = (TextView) findViewById(R.id.tvTitle);
        Typeface font = Typeface.createFromAsset(getAssets(),"fonts/Vazir.ttf");
        textViewTitle.setTypeface(font);

        editText = (EditText) findViewById(R.id.et1);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Toast.makeText(getBaseContext(),s , Toast.LENGTH_SHORT).show();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });



    }







}
